import 'package:flutter/cupertino.dart';
import 'package:doni/components/color_style.dart';
import 'package:google_fonts/google_fonts.dart';

//style for title
var mTitleStyle = TextStyle(
    fontFamily: "assets/fonts/Montserrat-Regular.ttf",
    fontWeight: FontWeight.w700,
    color: mTitleColor,
    fontSize: 18);
var mTitleHeaderStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mTitleColor, fontSize: 24);
//box style
var mHeaderStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w500, color: mTitleColor, fontSize: 12);
var mDescriptionStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w500, color: mSubtitleColor, fontSize: 10);
