import 'package:doni/Screens/dashboard/dashboard.dart';
import 'package:doni/Screens/dashboard/home.dart';
import 'package:doni/Screens/login/login.dart';
import 'package:doni/Screens/login/login_sukses/login_sukses.dart';
import 'package:doni/Screens/login/lupa_password/lupa_password.dart';
import 'package:doni/Screens/profile/edit.dart';
import 'package:doni/Screens/profile/profile.dart';
import 'package:flutter/widgets.dart';
import 'package:doni/Screens/splash/splash_screen.dart';

//semua route akses sistem
//dijadikan satu disini

final Map<String, WidgetBuilder> routes = {
  SplashScreen.namaRoute: (context) => SplashScreen(),
  LoginScreen.namaRoute: (context) => LoginScreen(),
  LupaPasswordScreen.namaRoute: (context) => LupaPasswordScreen(),
  LoginSuksesScreen.namaRoute: (context) => LoginSuksesScreen(),
  DashboardScreen.namaRoute: (context) => DashboardScreen(),
  Home.namaRoute: (context) => Home(),
  Profile.namaRoute: (context) => Profile(),
  Edit.namaRoute: (context) => Edit(),
};
