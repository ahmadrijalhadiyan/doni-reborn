import 'package:doni/Screens/login/login.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/components/size_config.dart';
import 'package:doni/components/system_setting.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int currentPage = 0;
  List<Map<String, String>> SplashData = [
    {
      "text": "Selamat datang di Digital Office Perhutani",
      "image": "assets/images/bg1.png",
    },
    {
      "text": "Aplikasi Surat Elektronik Penunjang Kerja",
      "image": "assets/images/bg2.png",
    },
    {
      "text": "Tetap Saling Terhubung dengan Rekan Kerja",
      "image": "assets/images/bg3.png",
    }
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                padding: new EdgeInsets.all(10),
                child: PageView.builder(
                  onPageChanged: (value) {
                    setState(() {
                      currentPage = value;
                    });
                  },
                  itemCount: SplashData.length,
                  itemBuilder: (context, index) => SplashWidget(
                    text: SplashData[index]["text"],
                    image: SplashData[index]["image"],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        SplashData.length,
                        (index) => buildDotitik(index: index),
                      ),
                    ),
                    Spacer(
                      flex: 3,
                    ),
                    DefaultBotton(
                      text1: "Lanjut",
                      press: () {
                        Navigator.pushNamed(context, LoginScreen.namaRoute);
                      },
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDotitik({int index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Colors.grey,
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}

class DefaultBotton extends StatelessWidget {
  const DefaultBotton({
    Key key,
    this.text1,
    this.press,
  }) : super(key: key);
  final String text1;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(40),
      child: FlatButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: kPrimaryColor,
        onPressed: press,
        child: Text(
          text1,
          style: TextStyle(
              fontSize: getProportionateScreenWidth(18), color: Colors.white),
        ),
      ),
    );
  }
}

class SplashWidget extends StatelessWidget {
  const SplashWidget({
    Key key,
    this.text,
    this.image,
  }) : super(key: key);
  final String text, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        Text(
          "DONI",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(36),
            color: kPrimaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(
          flex: 2,
        ),
        Image.asset(
          image,
          height: getProportionateScreenHeight(235),
          width: getProportionateScreenWidth(235),
        ),
      ],
    );
  }
}
