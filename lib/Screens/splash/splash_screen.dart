import 'package:flutter/material.dart';
import 'package:doni/components/size_config.dart';
import 'package:doni/Screens/splash/components/body.dart';

class SplashScreen extends StatelessWidget {
  static String namaRoute = "/splash";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}
