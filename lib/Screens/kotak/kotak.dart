import 'package:doni/Screens/kotak/components/body.dart';
import 'package:flutter/material.dart';
import 'package:doni/components/color_style.dart';

class Kotak extends StatefulWidget {
  @override
  _KotakState createState() => _KotakState();
}

class _KotakState extends State<Kotak> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: mBackgroundColor,
        appBar: new AppBar(
          title: Image.asset(
            "assets/images/Doni.png",
            height: 50,
          ),
          actions: <Widget>[
            Container(
                margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                child: IconButton(
                  icon: Icon(
                    Icons.notifications_none,
                    color: Color(0xFF545D68),
                    size: 30,
                  ),
                  onPressed: () {},
                )),
          ],
          centerTitle: true,
          backgroundColor: Colors.white,
          shadowColor: Colors.grey.withOpacity(.30),
          elevation: 10,
        ),
        body: Body());
  }
}
