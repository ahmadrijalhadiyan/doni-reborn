import 'package:doni/Screens/kotak_keluar/kotak_keluar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/Screens/dashboard/home.dart';
import 'package:doni/Screens/kotak/kotak.dart';
import 'package:doni/Screens/profile/profile.dart';

class DashboardScreen extends StatefulWidget {
  static String namaRoute = "/dashboard";
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _currentIndex = 0;
  var bottomTextStyle =
      GoogleFonts.inter(fontSize: 12, fontWeight: FontWeight.w500);
  final List<Widget> _children = [
    Home(),
    Kotak(),
    KotakKeluar(),
    Profile(),
  ];
  void onTappedBar(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      backgroundColor: mBackgroundColor,
      body: _children[_currentIndex],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.30),
              spreadRadius: 2,
              blurRadius: 15,
              offset: Offset(0, 5),
            )
          ],
          color: Colors.black,
        ),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          backgroundColor: colorScheme.surface,
          selectedItemColor: Color(0xFF2661FA),
          unselectedItemColor: colorScheme.onSurface.withOpacity(.60),
          selectedLabelStyle: textTheme.caption,
          unselectedLabelStyle: textTheme.caption,
          onTap: onTappedBar,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Beranda',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.mail),
              label: 'Kotak Masuk',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.markunread_mailbox),
              label: 'Kotak Keluar',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Akun',
            ),
          ],
        ),
      ),
    );
  }
}
