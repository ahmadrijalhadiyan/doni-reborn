import 'package:doni/Screens/login/login.dart';
import 'package:doni/Screens/profile/edit.dart';
import 'package:doni/Screens/profile/profile.dart';
import 'package:doni/components/size_config.dart';
import 'package:flutter/material.dart';

import 'package:doni/components/fonts_style.dart';
import 'package:doni/components/color_style.dart';

class Home extends StatefulWidget {
  static String namaRoute = "/home";
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: mBackgroundColor,
      appBar: new AppBar(
        title: Image.asset(
          "assets/images/Doni.png",
          height: 50,
        ),
        actions: <Widget>[
          Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: IconButton(
                icon: Icon(
                  Icons.notifications_none,
                  color: Color(0xFF545D68),
                  size: 30,
                ),
                onPressed: () {},
              )),
        ],
        centerTitle: true,
        backgroundColor: Colors.white,
        shadowColor: Colors.grey.withOpacity(.30),
        elevation: 10,
      ),
      body: SafeArea(
        child: new Container(
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      offset: Offset(4.0, 4.0),
                      blurRadius: 15,
                      spreadRadius: 2.0,
                    ),
                    BoxShadow(
                      color: Colors.grey[600],
                      offset: Offset(-4.0, -4.0),
                      blurRadius: 15,
                      spreadRadius: 2.0,
                    ),
                  ],
                ),
                child: Image.asset("assets/images/bg_doni.png"),
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Selamat datang, Rijal",
                      style: mTitleStyle,
                    ),
                  ),
                  Expanded(child: Container()),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, Edit.namaRoute);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Info",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.blue[400],
                            fontWeight: FontWeight.w600,
                            fontFamily: "assets/fonts/Montserrat-Regular.ttf"),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(
                      Icons.arrow_forward,
                      size: 18,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
