import 'package:flutter/material.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/components/fonts_style.dart';
import 'package:doni/Screens/profile/edit.dart';
import 'package:doni/Screens/profile/settings.dart';

class Profile extends StatefulWidget {
  static String namaRoute = "/profile";
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: mBackgroundColor,
      appBar: new AppBar(
        title: Image.asset(
          "assets/images/Doni.png",
          height: 50,
        ),
        actions: <Widget>[
          Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: IconButton(
                icon: Icon(
                  Icons.settings,
                  color: Color(0xFF545D68),
                  size: 30,
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Settings()));
                },
              )),
        ],
        centerTitle: true,
        backgroundColor: Colors.white,
        shadowColor: Colors.grey.withOpacity(.30),
        elevation: 10,
      ),
      body: new Container(
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16, top: 20, right: 16),
              child: Text(
                "Profile",
                style: mTitleHeaderStyle,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 4,
                        color: Theme.of(context).scaffoldBackgroundColor,
                      ),
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 2,
                            blurRadius: 2,
                            color: Colors.black.withOpacity(0.1),
                            offset: Offset(0, 5)),
                      ],
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/users.jpeg"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            buildTextField("No Induk Pegawai", "PHT12345xxxxxx"),
            buildTextField("Nama Lengkap", "Ahmad Rijal Hadiyan"),
            buildTextField("Email", "doni@perhutani.id"),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                  color: Colors.orangeAccent,
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Edit()));
                  },
                  child: Text(
                    "Edit",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildTextField(String labelText, String placeholder) {
    return Padding(
      padding: EdgeInsets.only(left: 36, top: 10, bottom: 12, right: 36),
      child: TextField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
