import 'package:doni/Screens/login/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/Screens/profile/profile.dart';
import 'package:doni/components/fonts_style.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: mBackgroundColor,
        appBar: new AppBar(
          title: Image.asset(
            "assets/images/Doni.png",
            height: 50,
          ),
          leading: IconButton(
            onPressed: () {
              //gunakan pop untuk kembali ke route
              Navigator.pop(
                  context, MaterialPageRoute(builder: (context) => Profile()));
            },
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xFF545D68),
              size: 30,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          shadowColor: Colors.grey.withOpacity(.30),
          elevation: 10,
        ),
        body: Container(
          child: ListView(
            physics: ClampingScrollPhysics(),
            padding: EdgeInsets.only(left: 16, top: 20, right: 16),
            children: <Widget>[
              Text(
                "Pengaturan",
                style: mTitleHeaderStyle,
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                children: [
                  Icon(
                    Icons.person,
                    color: mBlueColor,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Akun",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Divider(
                height: 15,
                thickness: 2,
              ),
              SizedBox(
                height: 8,
              ),
              buildPengaturanAkun(context, "Ubah Password"),
              buildPengaturanAkun(context, "Sosial Media"),
              buildPengaturanAkun(context, "Bahasa"),
              buildPengaturanAkun(context, "Privasi dan Keamanan"),
              SizedBox(
                height: 40,
              ),
              Row(
                children: [
                  Icon(
                    Icons.volume_up_outlined,
                    color: mBlueColor,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Notifikasi",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Divider(
                height: 15,
                thickness: 2,
              ),
              SizedBox(
                height: 8,
              ),
              buildNotifikasiAkun("Aktifitas", true),
              buildNotifikasiAkun("Berita", false),
              buildNotifikasiAkun("Mode Tampilan", true),
              SizedBox(
                height: 50,
              ),
              Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 4),
                        blurRadius: 5.0)
                  ],
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.0, 1.0],
                    colors: [
                      Color(0xFFFFA53E),
                      Color(0xFFFF7643),
                    ],
                  ),
                  color: Color(0xFFFFA53E),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ElevatedButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),

                    backgroundColor:
                        MaterialStateProperty.all(Colors.transparent),
                    // elevation: MaterialStateProperty.all(3),
                    shadowColor: MaterialStateProperty.all(Colors.transparent),
                  ),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginScreen()));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                      bottom: 10,
                    ),
                    child: Text(
                      "Sign Out",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Row buildNotifikasiAkun(String title, bool isActive) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: mGreyColor,
          ),
        ),
        Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value: isActive,
              onChanged: (bool val) {},
            )),
      ],
    );
  }

  GestureDetector buildPengaturanAkun(BuildContext context, String title) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text(
                  title,
                  textAlign: TextAlign.center,
                ),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("opetion 1"),
                    Text("opetion 2"),
                    Text("opetion 3"),
                  ],
                ),
                actions: [
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("close"),
                  ),
                ],
              );
            });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 9.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: mGreyColor,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: mGreyColor,
            )
          ],
        ),
      ),
    );
  }
}
