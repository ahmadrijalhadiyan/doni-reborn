import 'package:doni/Screens/profile/settings.dart';
import 'package:flutter/material.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/Screens/profile/profile.dart';
import 'package:doni/components/fonts_style.dart';

class Edit extends StatefulWidget {
  static String namaRoute = '/edit_profile';
  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: mBackgroundColor,
      appBar: new AppBar(
        title: Image.asset(
          "assets/images/Doni.png",
          height: 50,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(
                context, MaterialPageRoute(builder: (context) => Profile()));
          },
          icon: Icon(
            Icons.arrow_back,
            color: Color(0xFF545D68),
            size: 30,
          ),
        ),
        actions: <Widget>[
          Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: IconButton(
                icon: Icon(
                  Icons.settings,
                  color: Color(0xFF545D68),
                  size: 30,
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Settings()));
                },
              )),
        ],
        centerTitle: true,
        backgroundColor: Colors.white,
        shadowColor: Colors.grey.withOpacity(.30),
        elevation: 10,
      ),
      body: new Container(
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16, top: 20, right: 16),
              child: Text(
                "Edit Profile",
                style: mTitleHeaderStyle,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Center(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 4,
                        color: Theme.of(context).scaffoldBackgroundColor,
                      ),
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 2,
                            blurRadius: 2,
                            color: Colors.black.withOpacity(0.1),
                            offset: Offset(0, 5)),
                      ],
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/users.jpeg"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            buildTextField("No Induk Pegawai", "PHT12345xxxxxx", false),
            buildTextField("Nama Lengkap", "Ahmad Rijal Hadiyan", false),
            buildTextField("Email", "doni@perhutani.id", false),
            buildTextField("Password", "********", true),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                  color: Colors.green[400],
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                  onPressed: () {
                    Navigator.pop(context,
                        MaterialPageRoute(builder: (context) => Profile()));
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildTextField(
      String labelText, String placeholder, bool isPasswordTextField) {
    return Padding(
      padding: EdgeInsets.only(left: 36, top: 10, bottom: 12, right: 36),
      child: TextField(
        obscureText: isPasswordTextField ? showPassword : false,
        decoration: InputDecoration(
          suffixIcon: isPasswordTextField
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      showPassword = !showPassword;
                    });
                  },
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: Colors.grey,
                  ),
                )
              : null,
          contentPadding: EdgeInsets.only(),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: placeholder,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
