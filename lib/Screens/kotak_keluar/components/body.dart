import 'package:doni/Screens/login/login.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/components/fonts_style.dart';
import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  const Body({Key key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      children: <Widget>[
        SafeArea(
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 16, top: 20, bottom: 12),
                child: Text(
                  'Kotak Keluar',
                  style: mTitleHeaderStyle,
                ),
              ),
              Container(
                height: 600,
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Column(
                  children: [
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 8),
                              padding: EdgeInsets.only(left: 16),
                              decoration: BoxDecoration(
                                color: mFillColor,
                                borderRadius: BorderRadius.circular(20),
                                border:
                                    Border.all(color: mBorderColor, width: 1),
                              ),
                              child: Row(
                                children: <Widget>[
                                  new Image.asset(
                                    'assets/svg/proses.png',
                                    fit: BoxFit.contain,
                                    height: 150,
                                    width: 150,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 8),
                              padding: EdgeInsets.only(left: 16),
                              decoration: BoxDecoration(
                                color: mFillColor,
                                borderRadius: BorderRadius.circular(20),
                                border:
                                    Border.all(color: mBorderColor, width: 1),
                              ),
                              child: Row(
                                children: <Widget>[
                                  new Image.asset(
                                    'assets/svg/nota-dinas-keluar.png',
                                    fit: BoxFit.contain,
                                    height: 150,
                                    width: 150,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 8),
                              padding: EdgeInsets.only(left: 16),
                              decoration: BoxDecoration(
                                color: mFillColor,
                                borderRadius: BorderRadius.circular(12),
                                border:
                                    Border.all(color: mBorderColor, width: 1),
                              ),
                              child: Row(
                                children: <Widget>[
                                  new Image.asset(
                                    'assets/svg/surat-keluar.png',
                                    fit: BoxFit.contain,
                                    height: 150,
                                    width: 150,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginScreen()));
                            },
                            child: Container(
                              margin: EdgeInsets.only(right: 8),
                              padding: EdgeInsets.only(left: 16),
                              decoration: BoxDecoration(
                                color: mFillColor,
                                borderRadius: BorderRadius.circular(12),
                                border:
                                    Border.all(color: mBorderColor, width: 1),
                              ),
                              child: Row(
                                children: <Widget>[
                                  new Image.asset(
                                    'assets/svg/disposisi-keluar.png',
                                    fit: BoxFit.contain,
                                    height: 150,
                                    width: 150,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginScreen()));
                          },
                          child: Container(
                            margin: EdgeInsets.only(right: 8),
                            padding: EdgeInsets.only(left: 16),
                            decoration: BoxDecoration(
                              color: mFillColor,
                              borderRadius: BorderRadius.circular(12),
                              border: Border.all(color: mBorderColor, width: 1),
                            ),
                            child: Row(
                              children: <Widget>[
                                new Image.asset(
                                  'assets/svg/status.png',
                                  fit: BoxFit.contain,
                                  height: 150,
                                  width: 150,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
