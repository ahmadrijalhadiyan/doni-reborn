import 'package:doni/Screens/login/lupa_password/lupa_password.dart';
import 'package:doni/Screens/splash/components/body.dart';
import 'package:doni/components/buildsystem/utility.dart';
import 'package:doni/components/color_style.dart';
import 'package:doni/components/custom_icon.dart';
import 'package:doni/components/size_config.dart';
import 'package:doni/components/system_setting.dart';
import 'package:flutter/material.dart';
import 'package:doni/components/background.dart';
import 'package:doni/components/buildsystem/routes.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:doni/Screens/login/login_sukses/login_sukses.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatelessWidget {
  static String namaRoute = "/log_in";
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: SizedBox(
          width: double.infinity,
          child: Background(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: SizeConfig.screenWidth * 0.08,
                    ),
                    SizedBox(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        child: Image.asset("assets/images/Doni.png",
                            width: size.width),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.08,
                    ),
                    SignForm(),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.08,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        MediaSosial(
                          icon: "assets/icons/facebook-2.svg",
                          press: () =>
                              launch('https://www.facebook.com/perumperhutani'),
                        ),
                        MediaSosial(
                          icon: "assets/icons/twitter.svg",
                          press: () =>
                              launch('https://twitter.com/PerumPerhutani'),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(20),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Tidak Punya Akun?",
                          style: TextStyle(
                            fontSize: getProportionateScreenWidth(16),
                          ),
                        ),
                        GestureDetector(
                          onTap: () => launch(
                              "http://layanan.perhutani.co.id/helpdeskti/"),
                          child: Text(
                            " Daftar",
                            style: TextStyle(
                              fontSize: getProportionateScreenWidth(16),
                              color: mOrangeColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  String username;
  String password;
  bool tetapmasuk = false;
  final List<String> errors = [];
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          buildUsername(),
          SizedBox(
            height: getProportionateScreenHeight(40),
          ),
          buildPassword(),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          Row(
            children: <Widget>[
              Checkbox(
                  value: tetapmasuk,
                  activeColor: kPrimaryColor,
                  onChanged: (value) {
                    setState(() {
                      tetapmasuk = value;
                    });
                  }),
              Text("Tetap Masuk"),
              Spacer(),
              GestureDetector(
                onTap: () => Navigator.popAndPushNamed(
                    context, LupaPasswordScreen.namaRoute),
                child: Text(
                  "Lupa Password",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          formError(errors: errors),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          DefaultBotton(
            text1: "Log In",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                //jika sukses login
                Navigator.pushNamed(context, LoginSuksesScreen.namaRoute);
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildUsername() {
    OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide: BorderSide(color: kTextColor),
      gapPadding: 10,
    );
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => username = newValue,
      onChanged: (value) {
        if (value.isNotEmpty && errors.contains(kUsernameNullError)) {
          setState(() {
            errors.remove(kUsernameNullError);
          });
        } else if (value.length >= 4 &&
            errors.contains(kInvalidUsernameError)) {
          setState(() {
            errors.remove(kInvalidUsernameError);
          });
          return "";
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty && !errors.contains(kUsernameNullError)) {
          setState(() {
            errors.add(kUsernameNullError);
          });
          return "";
        } else if (value.length < 4 &&
            !errors.contains(kInvalidUsernameError)) {
          setState(() {
            errors.add(kInvalidUsernameError);
          });
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Username",
        hintText: "Masukan Username",
        //gunakan package svg versi terbaru dari 1.20.*
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomIcon(
          svgIcon: "assets/icons/Mail.svg",
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: outlineInputBorder,
        focusedBorder: outlineInputBorder,
        border: outlineInputBorder,
      ),
    );
  }

  TextFormField buildPassword() {
    OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide: BorderSide(color: kTextColor),
      gapPadding: 10,
    );
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty && errors.contains(kPassNullError)) {
          setState(() {
            errors.remove(kPassNullError);
          });
        } else if (value.length >= 5 && errors.contains(kShortPassError)) {
          setState(() {
            errors.remove(kShortPassError);
          });
          return "";
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty && !errors.contains(kPassNullError)) {
          setState(() {
            errors.add(kPassNullError);
          });
          return "";
        } else if (value.length < 5 && !errors.contains(kShortPassError)) {
          setState(() {
            errors.add(kShortPassError);
          });
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "Masukan Password",
        //gunakan package svg versi terbaru dari 1.20.*
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: CustomIcon(
          svgIcon: "assets/icons/Lock.svg",
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 42,
          vertical: 20,
        ),
        enabledBorder: outlineInputBorder,
        focusedBorder: outlineInputBorder,
        border: outlineInputBorder,
      ),
    );
  }
}

class MediaSosial extends StatelessWidget {
  const MediaSosial({
    Key key,
    this.icon,
    this.press,
  }) : super(key: key);
  final String icon;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10)),
        padding: EdgeInsets.all(getProportionateScreenWidth(12)),
        height: getProportionateScreenHeight(40),
        width: getProportionateScreenWidth(40),
        decoration: BoxDecoration(
          color: Color(0xFFF5F6F9),
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(icon),
      ),
    );
  }
}

class formError extends StatelessWidget {
  const formError({
    Key key,
    @required this.errors,
  }) : super(key: key);

  final List<String> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(
          errors.length, (index) => formErrorText(error: errors[index])),
    );
  }

  Row formErrorText({String error}) {
    return Row(
      children: [
        SvgPicture.asset(
          "assets/icons/Error.svg",
          height: getProportionateScreenHeight(14),
          width: getProportionateScreenWidth(14),
        ),
        SizedBox(
          height: getProportionateScreenWidth(10),
        ),
        Text(error),
      ],
    );
  }
}
