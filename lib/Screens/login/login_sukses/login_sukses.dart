import 'package:doni/Screens/dashboard/dashboard.dart';
import 'package:doni/Screens/dashboard/home.dart';
import 'package:doni/Screens/splash/components/body.dart';
import 'package:doni/components/size_config.dart';
import 'package:flutter/material.dart';

import '../login.dart';

class LoginSuksesScreen extends StatefulWidget {
  static String namaRoute = "/login_sukses";
  @override
  _LoginSuksesScreenState createState() => _LoginSuksesScreenState();
}

class _LoginSuksesScreenState extends State<LoginSuksesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text(
          "Login Sukses",
          style: TextStyle(color: Color(0xFF545D68)),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.popAndPushNamed(context, LoginScreen.namaRoute);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Color(0xFF545D68),
            size: 30,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        shadowColor: Colors.grey.withOpacity(.30),
        elevation: 10,
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.screenHeight * 0.04,
            ),
            Image.asset(
              "assets/images/success.png",
              height: SizeConfig.screenHeight * 0.4,
            ),
            SizedBox(
              height: SizeConfig.screenHeight * 0.08,
            ),
            Text(
              "Login Sukses",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(30),
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Spacer(),
            SizedBox(
              width: SizeConfig.screenWidth * 0.8,
              child: DefaultBotton(
                text1: "Kembali ke Beranda",
                press: () {
                  Navigator.pushNamed(context, DashboardScreen.namaRoute);
                },
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
