import 'package:doni/Screens/login/login.dart';
import 'package:doni/Screens/splash/components/body.dart';
import 'package:doni/components/size_config.dart';
import 'package:flutter/material.dart';
import 'package:doni/components/color_style.dart';
import 'package:url_launcher/url_launcher.dart';

class LupaPasswordScreen extends StatelessWidget {
  static String namaRoute = "/lupa_password";
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: mBackgroundColor,
      appBar: new AppBar(
        title: Text(
          "Lupa Password",
          style: TextStyle(color: Color(0xFF545D68)),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.popAndPushNamed(context, LoginScreen.namaRoute);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Color(0xFF545D68),
            size: 30,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        shadowColor: Colors.grey.withOpacity(.30),
        elevation: 10,
      ),
      body: Container(
        child: SizedBox(
          width: double.infinity,
          child: SafeArea(
            child: Column(
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(40),
                ),
                Image.asset(
                  "assets/images/reset_password.png",
                  height: SizeConfig.screenHeight * 0.17,
                ),
                SizedBox(
                  height: getProportionateScreenHeight(40),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(20)),
                  child: Text(
                    "Jika Anda Mengalami Kendala, dalam Login Aplikasi DONI, baik username, ataupun password, silahkan menghubungi tim IT.",
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Spacer(),
                SizedBox(
                  width: SizeConfig.screenWidth * 0.6,
                  child: DefaultBotton(
                    text1: "Hubungi Admin ",
                    press: () =>
                        launch("http://layanan.perhutani.co.id/helpdeskti/"),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
